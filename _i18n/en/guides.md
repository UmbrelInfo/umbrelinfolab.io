We are going to present you a series of mini guides on apps that you can use with the Umbrel node. We will try to update this page to keep up with new apps and features, so stay tuned, bookmark this page for later.

Table of contents
{: .no_toc .text-delta }
1. TOC
{:toc}

## **Umbrel + LNDHub + Bluewallet**

### **Description**
This is a major step forward for all node operators. Why? Because is one more step for a node in becoming a **“Bitcoin Sovereign Bank”**, being a HUB for your family, friends and all those who do not/can’t have a node for themselves. Practically you can “rent” them space and liquidity from your node.

More details about [LNDhub here](https://bluewallet.io/lndhub/) and also here is [the announcement from Bluewallet team](https://bluewallet.io/BlueWallet-brings-zero-configuration-Lightning-payments-to-iOS-and-Android-30137a69f071/).

There’s a very important aspect in Bitcoin world and few understand it:
- custodial wallets (your funds are in custody and control by other parties)
- private wallets (your funds are in your own custody and you are the only one control them)

**NOTE - For the moment mobile app BW WILL NOT manage your node/channels as Zeus or Zap do.** Right now it is activated only the part of LNDHub of your node. So if you activate and install this app now just to see your channels, **don’t expect to see them**.

FROM NOW ON YOU CAN BE THE BANK FOR YOUR FAMILY AND FRIENDS!

### **Misconceptions**

- **Bluewallet LNDHub is a wallet manager and NOT a node manager. It’s a big difference.**
- With your node and apps like Zeus, Zap, RTL, Thunderhub, you are managing “the bank”, your node, your channels, you are the banker, moving funds, offering liquidity for your own internal users.
- With BW LNDHub, you are managing the “customers” of your bank, their access to the liquidity of your bank, and managing their own “safety boxes” in your node.
- Think about that a LNDHub wallet is like an empty glass. Your LNDhub node is the provider for transporting the water to/from your glass of water. You get water into this glass from other sources (NOT from your LN node) and your node is transporting it (liquidity).

But be aware, own custody means also that you will have:
- more responsibility for your own funds, to keep safe and private your funds and wallets
- more knowledge, learning into how to use these new tools
- more costs (sometimes, if you do not know how to do it properly), mistakes costs, so you better learn first to do it properly
- more responsibility for those who will be using your node/walelts/funds
offer liquidity, this can end in more costs too

So if until now, if you were using BlueWallet (BW), you were using a custodial LN wallet (not onchain). That means that BW servers were in charge of managing your LN wallet, creating it and offering liquidity to be able to use sats on LN without bothering to run a node, maintaining it, open channels, keep the liquidity up etc Easy peasy

### **Usage for LNDHub**

- connect your mobile BW app to your LNDHub node
- create a LN wallet in your BW app
- that LN wallet will have 0 (zero) balance, but will be able to use the liquidity of your channels, NOT the balance (!)
- create as many wallets you want for your family and friends, they will manage their own wallets and balances, but keep in mind: you are responsible for their wallets to be online and be able to transact
- TXs between these lndhub wallets will be with zero fee, it will not go through the whole network of nodes, as a 3rd layer, only happen inside your node
- an interesting scenario could be as a restaurant offering to his clients option to have their own "restaurant lndhub wallets" and pay with zero fee, instant and private. These wallets can be refilled from external sources, from their own onchain wallets and they can pay wherever they want. When they pay to the restaurant, paying to another lndhub wallet from the same node will be zero fee.
- You can fund this new empty wallet from different sources:
   - BW onchain wallet - in wallet, click on "manage funds" and select the option "refill". It will bring up one of your onchain wallets you have setup in your BW app. If you have one. It could be imported (using the node seed) the onchain wallet from your node, but that is not recommended. You better use a separate one. Optional you can use an "external" wallet (in the same popup screen) and will generate an onchain invoice to be paid from another onchain wallet. That tx will be later moved into LN using some LN magic :)
   - any other LN external wallet (not from your node!), just making an invoice to be paid from your new lndhub wallet

### **How to connect BW to your node**

a. Update Bluewallet mobile app:
- On Android 9+ update to verion 6.1.1. You can use your [Play Store](https://play.google.com/store/apps/details?id=io.bluewallet.bluewallet) (if you still have it) or [BW Github source](https://github.com/BlueWallet/BlueWallet/releases) directly.
- On Android 8 or lower, stick with v.6.0.8 until new updates. Version 6.1.1 is restricted to Android versions lower than 8, becasue it has Tor integrated. You can continue to use v 6.0.8 with Orbot app (Tor) if you want, it works fine. I can say even better. V 6.1.1 on Android 9+ sometimes crashes due to Tor. Or if you can update your OS version, it will be good. If you can't, then it's time for you to buy a serious new phone. I recommend a Pixel and then install a clean new OS like GrapheneOS or CalyxOS on it. 
- On iOS, just go to App Store and do the update.

b. Update your Umbrel node to v 0.3.10 (you already know how)

c. Install BlueWallet app on Umbrel. Go to App Store and install it.

d. Connect your BW mobile app with your node. Open the section “Connect wallet” and open Bluewallet instructions. Open your BW mobile app and follow those instructions, step by step:
- in BW, go to Options - Network - enable Tor. Take a test. Restart BW (indicated by force close)
- in BW, go to Options - Network - Electrum Server. Scan the code of the address of your Electrum Server (onion) and the port 50001. Make a test. Restart BW
- When opening BW, if you already have a wallet created, wait for it to update, it may take a while. if the app crashes, restart again. Sometimes Tor does weird things on Android 9+ with v 6.1.1
- go to Options - Network - Lightning. Now you can scan the QR of your LNDhub node.
- go back to the main screen and add a new LN wallet. You just press create and voila, you already have a clean wallet connected to your node. This node has no funds, you will have to fund it from another LN wallet. This wallet ONLY uses the liquidity (not the balance) of your node to be able to send through LN. 

### **Testing the new wallets**

Payment using an external node wallet:
- Open that new wallet, create a 100 sats bill, copy the invoice code
- In the same BW mobile app, go to your usual BW LN wallet and click send, paste the invoice you created from your LNDhub wallet and send it. (this didn't work for me, it says "payment in transit", that is, I can't find the correct path between BW's servers and mine). Ok, this can happen.
- Create a new LN invoice for 120 sats in LNDhub wallet and go to [Telegram LNtxbot](https://telegra.ph/LNtxBot-Guide-01-24) and pay it. Also if you have the imported LNtxBot wallet in your BW, you can do it from there. It works perfectly.
- Go in Umbrel to RTL - LN - Transactions and in the invoices I can see the 120 sats received and also appear in the balance of my LNDhub wallet
- Reverse payment attempt. From LNDhub wallet, to LNtxBot wallet. So go to [LNtxbot LNurl](https://lntxbot.bigsun.xyz/@DarthCoin) and pay 100 sats + 1 sat fee with my Umbrel's LNDhub wallet
- Tx appear in RTL transactions immediately

Payment using 2 LNDhub wallets, on the same LNDhub server (your node):
- create two lndhub wallets (could be on the same BW app), but for convenience use two phones with BW connected to your LNDhub
- fund one of them (wallet A) from an external wallet (see previous scenario)
- in wallet B, create an invoice of 100 sats
- open wallet A and scan that invoice and pay it
- That's it! TX went through instant, with ZERO fee, full private
- for the moment the tx will appear in your LND apps (RTL/TH/Umbrel) as "unpaid" invoices, but are paid actually. Maybe in the future we will have a way to treat these txs as "internal tx" not as "invoices"

**EVERYTHING IS READY! IT WORKS WELL. EVEN MORE THINGS TO COME.**

NOTE: If you have issues or want to add something in regards of this guide, you are welcome to [Umbrel Community Forum](https://community.getumbrel.com/) where you can find even more guides, procedures, discussions and many people that can help.

---

## **Umbrel + LNBits**

### **Description**

LNBits it’s an amazing app that is adding a suite of features to Umbrel node and users can adapt to their needs.
This app is using the node liquidity (onchain and LN) and is adding a separate database for management.
You can use it for multiple wallets, as a LNDhub or specific usage for merchants, content creators, web content, LN ATMs and many others.

LNBits Links:
- [LNBits.com](https://lnbits.com/) - Public testing instance
- [LNBits.org](https://lnbits.org/) - More info and download source
- [Github page](https://github.com/lnbits/lnbits) - participate on open source code
- [LNBits Youtube channel](https://www.youtube.com/channel/UCGXU2Ae5x5K-5aKdmKqoLYg/videos) - with demos and tutorials
- [LNBits Telegram Group](https://t.me/lnbits) - where you can get more help directly from developers

NOTES:
- This Umbrel app works only behind Tor, so any extension you add/use/create QRs, you will need to use it from the .onion address provided. Do not share this onion address!
- If you want to use it on clearnet, [here is an amazing guide to follow](https://community.getumbrel.com/t/guide-lnbits-without-tor/604).
- Once you open/create a new LNBits wallet, save that address link into your bookmarks. There’s no other way (for the moment) to recover that wallet or login again. The address contain the key to the wallet. Do not share this address!
- LNBits is still beta phase, so take that in consideration and do not use it in “production” scenario.

### **Functionalities - extensions**

From app extensions we mention:
- [LNurl-pay](https://www.youtube.com/watch?v=WZpK4xfGcuY) - a simple way to have a static non-expiring LN QR code for receiving
- [LNurl-withdraw](https://www.youtube.com/watch?v=TUmsHpJtveQ) - nice way to create vouchers loaded with sats and others can just scan and withdraw to their LN wallets
- [TPoS](https://www.youtube.com/watch?v=6yNBbDWf-RE) - virtual LN PoS for small shops
- [LNDHub](https://www.youtube.com/watch?v=WWrAayAfqaQ) - create various LND wallets, for family, employees, customers with different levels of access
- [DJ Livestream](https://www.youtube.com/watch?v=zDrSWShKz7k) - create your own jukebox paid with sats
- [SatsPayServer](https://www.youtube.com/watch?v=p0QZe9yO64Y) - extension to create onchain and LN charges
- [Offline Shop](https://www.youtube.com/watch?v=_XAvM_LNsoo) - Receive payments for products offline
- Events - Sell and register event tickets
- Captcha - Create captcha to stop spam, paid with sats
- [Paywall](https://www.youtube.com/watch?v=N0fU_0gbaXs) - Create paywalls for content
- Subdomains - Sell subdomains of your domain
- [Support tickets](https://www.youtube.com/watch?v=iq6BLfLsU6Y) - LN support ticket system
- [Watch only](https://www.youtube.com/watch?v=Xx09tzeZIok) - Add your cold wallets in watch-only mode with total privacy and security
- [User Manager](https://www.youtube.com/watch?v=E-IjtCHUVBE) - Generate users and wallets
- Bleskomat - Connect a Bleskomat LN ATM to an lnbits. 
    - [Tutorial how to build FOSSA LN ATM](https://www.youtube.com/watch?v=1FeE74sDNGA)
    - [DIY Bleskomat LN ATM](https://github.com/samotari/bleskomat) and [here more examples](https://degreesofzero.com/article/beyond-coffee-bitcoin-for-every-day-use-with-lnurl.html)
    - [Demo how to use a LN ATM](https://www.youtube.com/watch?v=DGFVhgHwt3E)
    - [Here is an open source documentation](https://docs.lightningatm.me) about how to build your own LN ATM and then connect it to your LNBits server for management.

And many more extensions to come!

### **Usage example**

This is just an example of how to connect LNbits wallet, for more using examples please consult [LNBits Youtube channel](https://www.youtube.com/channel/UCGXU2Ae5x5K-5aKdmKqoLYg/videos)
- Install in Umbrel the app LNBits. This will activate the option to create various LN wallets on your node, that can be linked using lndhub connection.
- Create a wallet in your LNbits module, activate the LNDHUB extension and link it to that LNbits wallet. Use the onion address for this procedure, NOT the local IP or umbrel.local. The wallet have to be visible over internet and the only way is through Tor.
- Save in your bookmarks that wallet address, because is the only way to open it again, the link contain the key to the wallet. New LNbits releases will have a login/authentification page, but for the moment save that wallet link.
- Open your Bluewallet, go to add wallet, click import and scan the QR code displayed on your LNbits LNDHUB. You have two options: as admin (full rights over the wallet) or invoicing only (limited rights only for creating invoices).
- **REMEMBER**: Use Bluewallet under Tor service otherwise will not read that QR code. [See more details here](https://info.umbrel.tech/guides.html#umbrel--lndhub--bluewallet).
- Done. Now you can use that wallet as a normal LN wallet in Bluewallet, no need for opening channels etc it’s all linked with your LN node, so it depends of your node liquidity. This is a good option if you have a small shop with employees that can charge using LN, without having full permissions to your node.

This post will be updated with more content, based on new features or additional information is required.

NOTE: If you have issues or want to add something in regards of this guide, you are welcome to [Umbrel Community Forum](https://community.getumbrel.com/) where you can find even more guides, procedures, discussions and many people that can help.

---

This guide was created by [DarthCoin](https://bitcoinhackers.org/@DarthCoin). If you want to test your new Umbrel node by tipping DarthCoin, you can generate a [LNUrl here](https://lntxbot.bigsun.xyz/@DarthCoin). The LNUrl can be read with the BlueWallet, Zeus or Zap wallet or the Thunderhub app within Umbrel.
