---
layout: default
title: titles.troubleshooting
nav_order: 10
---

# Umbrel is lying to you

Umbrel claims it's free and open, while in reality, it's not.

There are alternatives which are easy to use, and have many more features.

Check out [runcitadel](https://runcitadel.space) for an upgrade with dark mode and lightning addresses (+ higher speed)
